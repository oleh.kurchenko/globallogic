#!/bin/bash

find ./ -maxdepth 1 -type f -a -ctime +30 -printf '%P\n' | xargs -I {} mv {} ~{}