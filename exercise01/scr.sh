#!/bin/bash

find $1 -maxdepth 1 -type f -name "[-~_]*" -o -type f -name "*.tmp" | xargs --no-run-if-empty rm
